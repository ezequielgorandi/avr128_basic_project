#ifndef _SPI_H_
#define _SPI_H_
#include <avr/io.h>

#define PORT_SPI    PORTB
#define DDR_SPI     DDRB
#define DD_MISO     DDB3
#define DD_MOSI     DDB2
#define DD_SS       DDB0
#define DD_SCK      DDB1

 void spi_init();
 void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len);
 void spi_transmit_sync (uint8_t * dataout, uint8_t len);
 uint8_t spi_fast_shift (uint8_t data);
 void spiSendByte (char databyte);

#endif /* _SPI_H_ */