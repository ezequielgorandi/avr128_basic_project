/*------------------------------------------------------------------------*/
#ifndef _timerStructure_
#define _timerStructure_
/*------------------------------------------------------------------------*/
#include <avr/io.h>

typedef struct ///< Referentes a los timers del del programa 
{
   uint16_t Tiempo;
   uint8_t En_uso;
} 
TIMERS_INTERNOS;

//! Definición de los timeouts que utilizan el timer 0
typedef enum {
	DELAY_0 = 0,
	SEND_COMMAND_TIMEOUT,	
	UART_RX_TIMER,
	CANT_TIMER_0
} TIMEOUT_0;

typedef enum {
	DELAY_1 = 0,
	UART_TIMEOUT,
	CANT_TIMER_1
} TIMEOUT_1;
/*------------------------------------------------------------------------*/
#endif
/*------------------------------------------------------------------------*/

