/**
@date 24/05/2017
@defgroup gorandi_timer Timer_0
@note
Microcontrolador: Atmega128
Configuración del timer 0.
	- 1KHz Tick
	- Normal Mode
	- Overflow interruption
*/
#include <avr/io.h>
#include <avr/interrupt.h>
#include "timerStructure.h"
#include "../../constants.h"


void init_Timer0(void);
void setTimeOut_Timer0(TIMEOUT_0 timer, unsigned int valor);
void setTimeOut_mS_Timer0(TIMEOUT_0 timer, uint16_t valor);
unsigned char timeOut_Timer0(TIMEOUT_0 timer);
void delay_Timer0(uint32_t tiempo);
void delay_ms_Timer0(uint32_t tiempo);
unsigned char getState_Timer0(TIMEOUT_0 timer);


/**************************** TIMER 0 *****************************************/

/* Base de tiempo*/
#define BASE_TIEMPO_0      1

#define TIMER0_COUNT  0x83  //
#define TIMER0_PRESCALER  0x04  //< Configuración del prescaler

#define TIMER0_TCCR0 0x00 //< OCCR Disconneted
#define TIMER0_TCCR0 0x00 //< No PWM Mode
#define TIMER0_ON (TIMER0_TCCR0 | TIMER0_PRESCALER) //< Start Timer
#define TIMER0_OFF (TIMER0_TCCR0 & ~TIMER0_PRESCALER)
#define TIMER0_SYNC_INTERNAL_MODE 0x00
#define TIMER0_INTERRUPT_ON TIMSK | (1<<TOIE0) //< Enable Timer Interrupt

/**************************** FIN TIMER 0 *************************************/

/**@{*/