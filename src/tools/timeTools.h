/*
 * timeTools.h
 *
 * Created: 10/11/2017 12:08:35 p.m.
 *  Author: Ezequiel
 */ 

char getStringDate( struct tm *timeStruct, char* buffer);

/**
 @brief Transforma el horario de timeStruct en un string del tipo HHMMSS
 @param struct tm*. Estructura time
 @param char*. Bufer donde se guarda el string
 @return char. Devuelve 0.
 @note 
*/
char getStringTime( struct tm *timeStruct, char* buffer);

char getStringTime_fromTimestamp( time_t *timestamp, char* buffer);

char getStringDate_fromTimestamp( time_t *timestamp, char* buffer);