/*------------------------------------------------------------------------*/
#ifndef _CTES_
#define _CTES_
/*------------------------------------------------------------------------*/

#ifndef FALSE
#define FALSE  0
#endif

#ifndef TRUE
#define TRUE  1
#endif

#define NEW_LINE "\n\r"

#ifndef ERROR
//#define ERROR 0
#endif

#ifndef OK
#define OK 1
#endif

#define HIGH 1
#define LOW  0

#define ENABLE 1
#define DISABLE  0


/* Usados por comodidad */
typedef unsigned char byte;
typedef unsigned char uchar;
typedef unsigned int  uint;
typedef unsigned long ulong;
#endif